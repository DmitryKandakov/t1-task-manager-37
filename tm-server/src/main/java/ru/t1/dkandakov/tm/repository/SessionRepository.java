package ru.t1.dkandakov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.constant.IDBConstant;
import ru.t1.dkandakov.tm.api.repository.ISessionRepository;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Getter
    @NotNull
    private final String tableName = IDBConstant.TABLE_SESSION;

    @NotNull
    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Session fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(row.getString(IDBConstant.ROW_ID));
        session.setDate(row.getTimestamp(IDBConstant.CREATED));
        session.setUserId(row.getString(IDBConstant.USER_ID));
        session.setRole(Role.valueOf(row.getString(IDBConstant.ROLE)));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?);",
                getTableName(),
                IDBConstant.ROW_ID, IDBConstant.CREATED,
                IDBConstant.USER_ID, IDBConstant.ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
        }
        return session;
    }

    @Nullable
    @Override
    public Session add(@Nullable final String userId, @Nullable final Session session) {
        session.setUserId(userId);
        return add(session);
    }

    @Override
    @SneakyThrows
    public Session update(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), IDBConstant.CREATED, IDBConstant.USER_ID,
                IDBConstant.ROLE, IDBConstant.ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
        return session;
    }

}