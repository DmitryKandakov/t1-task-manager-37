package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataYamlLoadFasterXmlRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Load data from yaml file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        getDomainEndpoint().loadDataYamlFasterXml(new DataYamlLoadFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return "data-load-yaml";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}