package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void removeAll(@NotNull String userId);

    boolean existsById(@Nullable String UserId, @NotNull String Id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String useId, @NotNull String Id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @NotNull Integer index);

    int getSize(@NotNull String userId);

    @Nullable
    M removeOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M add(final @Nullable String UserId, @Nullable M model);

    @Nullable
    M removeOne(final @NotNull String userId, @NotNull M model);

}