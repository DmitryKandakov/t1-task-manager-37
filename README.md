# TASK MANAGER

## DEVELOPER INFO

**NAME**: Dmitry Kandakov

**E-MAIL**: dkandakov@t1-consulting.ru

**E-MAIL**: dkandakov@yandex.ru

## SOFTWARE

**OS**: WINDOWS 10

**JDK**: OPENJDK 1.8.0_322

## HARDWARE

**CPU**: i5

**RAM**: 16GB

**SSD**: 512GB

## BUILD PROGRAM

```shell
mvn clean install
```

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
