package ru.t1.dkandakov.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFildException {

    public DescriptionEmptyException() {
        super("Error! Project description is empty...");
    }

}
